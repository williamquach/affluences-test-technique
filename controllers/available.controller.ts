import Http from 'http';

export class AvailableController {


    private static instance: AvailableController;

    public static async getInstance(): Promise<AvailableController> {
        if(this.instance === undefined) {
            AvailableController.instance = new AvailableController();
        }
        return AvailableController.instance;
    }

    public async isResourceAvailable(date: string, resourceId: string): Promise<boolean> {
        let resource: boolean = false;

        // Is resource open
        // Is datetime available
        let reservations = this.getReservations(date, resourceId);

        return resource;
    }

    private isOpen(date: string, resourceId: string) {
        console.log(date);
        const parameters = `?date=${date}&resourceId=${resourceId}`;
        console.log(parameters);
        const options = {
            hostname: 'localhost',
            port: 8080,
            path: '/timetables' + parameters,
            method: 'GET',
        }

        const getReservations = Http.request(options, res => {
            console.log(`statusCode: ${res.statusCode}`)
            console.log(`res: ${res}`)

            res.on('data', d => {
                process.stdout.write(d)
            })

        })

        getReservations.on('error', error => {
            console.error(error)
        })

        getReservations.end()


        const isOpen = false;
        return isOpen;
    }

    private getReservations(date: string, resourceId: string) {
        const parameters = `?date=${date}&resourceId=${resourceId}`;
        console.log(parameters);
        const options = {
            hostname: 'localhost',
            port: 8080,
            path: '/reservations' + parameters,
            method: 'GET',
        }

        const getReservations = Http.request(options, res => {
            console.log(`statusCode: ${res.statusCode}`)
            console.log(`res: ${res}`)

            res.on('data', d => {
                process.stdout.write(d)
            })

        })

        getReservations.on('error', error => {
            console.error(error)
        })

        getReservations.end()
    }

}
