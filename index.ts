import {buildRoutes} from "./routes";
import express, {Express} from "express"
import bodyParser from "body-parser"

const app: Express = express();

app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());

buildRoutes(app);

const port = process.env.PORT || 3000;
app.listen(port, function() {
    console.log(`Listening on ${port}....`);
});
