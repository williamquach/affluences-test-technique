import {Express} from "express";
import {availableRouter} from "./available.router";

export function
buildRoutes(app: Express) {
    app.use("/available", availableRouter);
}

