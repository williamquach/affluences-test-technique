import express from 'express';
import {AvailableController} from "../controllers/available.controller";

const availableRouter = express.Router();


availableRouter.get("/", async function(request, result) {
    let dateParam = request.query.date;
    const resourceId = request.query.resourceId;

    if(dateParam == undefined || resourceId == undefined) {
        result.status(400)
            .send(`Missing parameters (date or resourceId)`)
            .end();
        return;
    }
    const availableController: AvailableController = await AvailableController.getInstance();

    const available: boolean = await availableController.isResourceAvailable(dateParam.toString(), resourceId.toString());


    if(available === null) {
        result.status(404).end();
    }
    else {
        result.json({"available": available});
    }
});

export function formattedDate(d = new Date): string {
    let month = String(d.getMonth() + 1);
    let day = String(d.getDate());
    const year = String(d.getFullYear());

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return `${year}-${day}-${month}`;
}

export {
    availableRouter
}
